terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  description = "DO Access Token"
  type        = string
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "asd" {
  name = "asd"
}

resource "digitalocean_droplet" "droplets" {
  count = 2
  image = "ubuntu-20-10-x64"
  name = "ansible-${count.index}"
  region = "fra1"
  size = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.asd.id]
}

resource "digitalocean_loadbalancer" "loadbalancer" {
  name = "loadbalancersh"
  region = "fra1"

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 5000
    target_protocol = "http"
  }

  healthcheck {
    port = 5000
    protocol = "http"
    path = "/"
  }

  droplet_ids = digitalocean_droplet.droplets.*.id
}

resource "digitalocean_record" "record" {
  domain     = "wheelet.me"
  type       = "A"
  name       = "terraform"
  value      = digitalocean_loadbalancer.loadbalancer.ip
}


output "droplets" {
  value = [for item in digitalocean_droplet.droplets : {
    host = item.ipv4_address
    name = item.name
  }]
}



