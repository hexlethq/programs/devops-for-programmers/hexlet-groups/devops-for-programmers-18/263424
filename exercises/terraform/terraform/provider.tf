terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  description = "DO Access Token"
  type        = string
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "asd" {
  name = "asd"
}

resource "digitalocean_droplet" "droplet-1" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-01"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.asd.id]
}

resource "digitalocean_droplet" "droplet-2" {
  image    = "docker-20-04"
  name     = "web-terraform-homework-02"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.asd.id]
}

resource "digitalocean_loadbalancer" "loadbalancer" {
  name = "loadbalancerr"
  region = "ams3"
  size = "lb-large"

  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 5000
    target_protocol = "http"
  }

  healthcheck {
    port = 5000
    protocol = "http"
    path = "/"
  }

  droplet_ids = [
    digitalocean_droplet.droplet-1.id,
    digitalocean_droplet.droplet-2.id
  ]
}

resource "digitalocean_record" "record" {
  domain     = "wheelet.me"
  type       = "A"
  name       = "terraform"
  value      = digitalocean_loadbalancer.loadbalancer.ip
}


output "droplets_ips" {
  value = [
    digitalocean_droplet.droplet-1.ipv4_address,
    digitalocean_droplet.droplet-2.ipv4_address
  ]
}
