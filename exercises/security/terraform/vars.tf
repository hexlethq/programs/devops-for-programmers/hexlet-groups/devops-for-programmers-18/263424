variable "do_token" {
  type        = string
}

variable "pvt_key" {
  type        = string
  default     = "~/.ssh/id_rsa"
}
