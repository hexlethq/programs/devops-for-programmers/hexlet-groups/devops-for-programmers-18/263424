resource "digitalocean_droplet" "bastion" {
  image              = "ubuntu-20-04-x64"
  name               = "bastion"
  region             = "fra1"
  size               = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys           = [data.digitalocean_ssh_key.asd.id]
  vpc_uuid = digitalocean_vpc.vpc.id
}

resource "digitalocean_firewall" "firewall" {
  name = "firewall"

  droplet_ids = [digitalocean_droplet.bastion.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol           = "icmp"
    source_droplet_ids = digitalocean_droplet.servers.*.id
  }

  outbound_rule {
    protocol                = "tcp"
    port_range              = "22"
    destination_droplet_ids = digitalocean_droplet.servers.*.id
  }

  outbound_rule {
    protocol                = "icmp"
    destination_droplet_ids = digitalocean_droplet.servers.*.id
  }
}

